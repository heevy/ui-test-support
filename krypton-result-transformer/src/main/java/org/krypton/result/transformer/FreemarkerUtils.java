package org.krypton.result.transformer;

import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * Created by jdk on 12.01.14.
 */
public class FreemarkerUtils {
    public String getStacktrace(Throwable e){
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        pw.flush();
        return sw.toString();
    }
}
