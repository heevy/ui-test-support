package org.krypton.result.transformer;

import com.thoughtworks.xstream.XStream;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.lang3.StringUtils;
import org.krypton.storage.TestEnvironment;
import org.krypton.storage.TestResults;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.util.ClassUtils;
import org.springframework.util.SystemPropertyUtils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 06.07.13
 * Time: 11:00
 * To change this template use File | Settings | File Templates.
 */
public class ResultTransformer {

    public static final Logger LOG = LoggerFactory.getLogger(ResultTransformer.class);

	private static Configuration cfg;

	// TODO Joudek Use spring for it or put it to better place
	static {
		/* Create and adjust the configuration */
		cfg = new Configuration();

		cfg.setClassForTemplateLoading(ResultTransformer.class, "/");

		cfg.setObjectWrapper(new DefaultObjectWrapper());
		cfg.setDefaultEncoding("UTF-8");
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
	}

	public void transform(String aResultDir) {
		File dir = new File(aResultDir);
		Iterator<File> testResultFiles =
				FileUtils.iterateFiles(dir, FileFilterUtils.nameFileFilter("krypton-test-result.xml"), FileFilterUtils.trueFileFilter());
        //TODO probably refactor in case of many tests and replace with some domain object
        List<TestResults> tests =new ArrayList<>();

		while (testResultFiles.hasNext()) {
            File testResultFile = testResultFiles.next();
            System.out.println(testResultFile.getAbsolutePath());
            String xml;
			try {
				xml = FileUtils.readFileToString(testResultFile, "UTF-8");

			} catch (IOException e) {
				throw new RuntimeException("Cannot read file '" + testResultFile.getAbsolutePath() + "'.", e);
			}
			XStream xstream = new XStream();
			TestResults results = (TestResults) xstream.fromXML(xml);
            File resHmtl=null;
			try {
				Template temp = cfg.getTemplate("/org/krypton/result/templates/testCase.ftl");
                int wholepath = StringUtils.countMatches(testResultFile.getParent(), File.separator);
                int startPath = StringUtils.countMatches(dir.getPath(),File.separator);

                StringBuilder sb = new StringBuilder();
                for(int i=1; i <= (wholepath-startPath); i++){
                    sb.append("../");
                }
                sb.deleteCharAt(sb.length()-1);

                Map<String, Object> freemarkerModel = new HashMap<>();
                freemarkerModel.put("testResult",results);
                freemarkerModel.put("resourcePath",sb.toString());
                freemarkerModel.put("utils", new FreemarkerUtils());
                StringWriter out = new StringWriter();
				temp.process(freemarkerModel, out);
                resHmtl = new File(testResultFile.getParentFile(),"result.html");
                FileUtils.write(resHmtl,out.toString());
			} catch (TemplateException e) {
				throw new RuntimeException("Cannot process template.", e);
			} catch (IOException e) {
				throw new RuntimeException("Cannot process template.", e);
			}
            String relativeResultPath = dir.toURI().relativize(resHmtl.toURI()).getPath();
            results.addTestStorage("TestTransformerResult",relativeResultPath);
            tests.add(results);
		}

        Collections.sort(tests, new Comparator<TestResults>() {
            @Override
            public int compare(TestResults o1, TestResults o2) {
                int res = o1.getName().compareTo(o2.getName());
                if(res==0){
                    TestEnvironment te1 = o1.getTestStorage(TestEnvironment.class.getSimpleName(),TestEnvironment.class);
                    TestEnvironment te2 = o2.getTestStorage(TestEnvironment.class.getSimpleName(),TestEnvironment.class);
                    res = te1.getBrowserName().compareTo(te2.getBrowserName());
                    if(res==0){
                        res = te1.getBrowserVersion().compareTo(te2.getBrowserVersion());
                    }
                }
                return res;
            }
        });
        

		/* Create a data-model */
		Map root = new HashMap();
		root.put("tests", tests);

		/* Get the template */
		Template temp = null;
		try {
			temp = cfg.getTemplate("/org/krypton/result/templates/testCases.ftl");
		} catch (IOException e) {
			throw new RuntimeException("Cannot load transformation template.", e);
		}

		/* Merge data-model with template */
		StringWriter out = new StringWriter();
		try {
			temp.process(root, out);
		} catch (TemplateException e) {
			throw new RuntimeException("Cannot process template.", e);
		} catch (IOException e) {
			throw new RuntimeException("Cannot process template.", e);
		}
		try {
			FileUtils.write(new File(dir, "testResults.html"), out.toString());
		} catch (IOException e) {
			throw new RuntimeException("Cannot write ui test result report.", e);
		}
        try {
            copyPackage("org.krypton.result.templates.resources",dir);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void copyPackage(String basePackage, File targetDir) throws IOException, ClassNotFoundException {
        List<ItemToCopy> items = findResources(basePackage);
        LOG.info("Copy resources package '{}' to directory '{}'.", basePackage,targetDir.getAbsolutePath());
        for(ItemToCopy i : items){
            String path = i.getPath();
            if(path.isEmpty()){
                continue;
            }
            File targetFile = new File(targetDir,path.replace("/",File.separator));
            LOG.info("Copy resource '{}' to directory '{}'.", path,targetFile.getAbsolutePath());
            targetFile.getParentFile().mkdirs();
            targetFile.createNewFile();
            try(InputStream is = i.getResource().getInputStream();OutputStream os = new FileOutputStream(targetFile)){
                org.apache.commons.io.IOUtils.copyLarge(is,os);
            }
        }
    }

    private List<ItemToCopy> findResources(String basePackage) throws IOException, ClassNotFoundException {
        ResourcePatternResolver resourcePatternResolver = new PathMatchingResourcePatternResolver();

        List<ItemToCopy> result = new ArrayList<>();
        String packageSearchPath = ResourcePatternResolver.CLASSPATH_ALL_URL_PREFIX + resolveBasePackage(basePackage) + "/" + "**";
        Resource[] resources = resourcePatternResolver.getResources(packageSearchPath);
        for (Resource resource : resources) {
            String path = resource.getURL().toString();
            String resultPath = path.replaceAll("^.*" + basePackage + "/(.*)$", "$1");
            resultPath = URLDecoder.decode(resultPath, "UTF-8");
            ItemToCopy item = new ItemToCopy(resultPath, resource);
            if(resultPath.endsWith("/")){
                continue;
            }
            result.add(item);
        }
        return result;
    }

    private String resolveBasePackage(String basePackage) {
        return ClassUtils.convertClassNameToResourcePath(SystemPropertyUtils.resolvePlaceholders(basePackage));
    }

    private static class ItemToCopy implements Comparable {
        private String path;

        private Resource resource;

        private ItemToCopy(String path, Resource content) {
            this.path = path;
            this.resource = content;
        }

        private String getPath() {
            return path;
        }

        private Resource getResource() {
            return resource;
        }

        @Override
        public int compareTo(Object o) {
            return path.compareTo(((ItemToCopy) o).path);
        }
    }

	public static void main(String[] args) {
		String outputFolder = System.getProperty("krypton.result.dir");
		if (outputFolder == null || outputFolder.isEmpty()) {
            System.err.println("Cannot run transformtion. Directory parameter is empty.");
            return;
		}
		new ResultTransformer().transform(outputFolder);
	}
}
