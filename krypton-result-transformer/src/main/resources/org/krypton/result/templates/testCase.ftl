<html>
<head>
    <title>${testResult.name}</title>
    <link rel="stylesheet" href="${resourcePath}/stylesheets/style.css"/>
    <link rel="stylesheet" href="${resourcePath}/stylesheets/colorbox.css"/>
    <link rel="stylesheet" href="${resourcePath}/bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="${resourcePath}/bootstrap/css/bootstrap-theme.css"/>
    <link rel="stylesheet" href="${resourcePath}/stylesheets/jquery.dataTables.bootstrap.css"/>
    <script src="${resourcePath}/javascripts/jquery-2.0.3.js"></script>
    <script src="${resourcePath}/bootstrap/js/bootstrap.js"></script>
    <script src="${resourcePath}/javascripts/jquery.dataTables.js"></script>
    <script src="${resourcePath}/javascripts/jquery.dataTables.bootstrap.js"></script>
    <script src="${resourcePath}/javascripts/jquery.colorbox.js"></script>
</head>
<body>
<div class="container">
    <h1>Test Result</h1>
    <dl class="dl-horizontal">
        <dt>Testcase :</dt>
        <dd>${testResult.name}</dd>
        <dt>Browser :</dt>
        <dd>${testResult.testStorage.TestEnvironment.browserName} ${testResult.testStorage.TestEnvironment.browserVersion}</dd>
        <dt>Platform :</dt>
        <dd>${testResult.testStorage.TestEnvironment.platform}</dd>
        <dt>Result :</dt>
        <dd>
            <#if testResult.testStorage.TestResult.finished>
                <span class="btn btn-success">PASSED</span>
            <#else>
                <span class="btn btn-danger">ERROR</span>
            </#if>
        </dd>
        <#if !testResult.testStorage.TestResult.finished>
            <dt>Exception :</dt>
            <dd><pre>${utils.getStacktrace(testResult.testStorage.TestResult.exception)}</pre></dd>
        </#if>
    </dl>
    <table id="testResult" class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0"
           border="0">
        <thead>
        <tr>
            <th>Step</th>
            <th>Screenshot</th>
        </tr>
        </thead>
    <#list testResult.commandStorage as step>
        <tr>
            <td>${step.command}</td>
            <td>
                <#if step.executed && step.storage.SnapshotStorage??>
                    <a class="krypton-screenshots" href="data/${step.storage.SnapshotStorage.screenshot}" title="${step.command}"><br/>
                        <img class="krypton-screenshot" src="data/${step.storage.SnapshotStorage.screenshot}">
                    </a>
                    <br/>
                    <br/>
                    <a href="data/${step.storage.SnapshotStorage.source}">Source HTML</a>
                </#if>
            </td>
        </tr>
    </#list>
    </table>
</div>
<script>
    $(document).ready(function () {
        $('#testResult').dataTable({'iDisplayLength': 100});
        $('.krypton-screenshots').colorbox({rel:".krypton-screenshots"});
    });
</script>
</body>
</html>
