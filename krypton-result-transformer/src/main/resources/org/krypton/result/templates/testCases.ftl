<html>
<head>
    <title>UI Tests</title>
    <link rel="stylesheet" href="stylesheets/style.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap.css"/>
    <link rel="stylesheet" href="bootstrap/css/bootstrap-theme.css"/>
    <link rel="stylesheet" href="stylesheets/jquery.dataTables.bootstrap.css"/>
    <script src="javascripts/jquery-2.0.3.js"></script>
    <script src="bootstrap/js/bootstrap.js"></script>
    <script src="javascripts/jquery.dataTables.js"></script>
    <script src="javascripts/jquery.dataTables.bootstrap.js"></script>
</head>
<body>
<div class="container">
    <h1>UI Test Results</h1>
<table id="testResults" class="table table-striped table-bordered table-hover" cellpadding="0" cellspacing="0" border="0">
    <thead>
    <tr>
        <th>Test Case</th>
        <th>Browser</th>
        <th>Result</th>
    </tr>
    </thead>
<#list tests as test>
    <tr>
        <td><a href="${test.testStorage.TestTransformerResult}">${test.name}</a></td>
        <td>${test.testStorage.TestEnvironment.browserName}</td>
        <td>
            <#if test.testStorage.TestResult.finished>
                <span class="btn btn-success">PASSED</span>
            <#else>
                <span class="btn btn-danger">ERROR</span>
            </#if>
    </tr>
</#list>
</table>
</div>
<script>
    $(document).ready(function() {
        $('#testResults').dataTable({'iDisplayLength': 100});
    } );
</script>
</body>
</html>
