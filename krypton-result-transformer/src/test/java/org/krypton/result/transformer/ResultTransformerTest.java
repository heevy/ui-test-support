package org.krypton.result.transformer;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

import java.io.File;
import java.util.UUID;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 16.07.13
 * Time: 22:25
 * To change this template use File | Settings | File Templates.
 */
public class ResultTransformerTest {

        @Test
        public void testTransformation() throws Exception{
            File result = new File(FileUtils.getTempDirectory(), "krypton-"+System.currentTimeMillis());
            result.mkdir();
            File resDir = new File("/Users/jdk/git/ui-test-support/krypton-result-transformer/src/test/resources/org/krypton/result/transformer/data");
            FileUtils.copyDirectory(resDir,result);
            System.out.println("Test directory : "+result.getAbsolutePath());
            ResultTransformer rt = new ResultTransformer();
            rt.transform(result.getAbsolutePath());

        }
}
