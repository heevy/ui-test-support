package org.krypton.test;

import org.junit.runners.Parameterized;
import org.krypton.junit.SeleniumTestType;
import org.krypton.junit.UITestParent;
import org.krypton.support.KryptonTestExecutor;
import org.openqa.selenium.WebDriver;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 22.06.13
 * Time: 14:52
 * To change this template use File | Settings | File Templates.
 */
//@Ignore
public class UITestTestSeznam extends UITestParent {


    public UITestTestSeznam(String name, String testCase, SeleniumTestType aType, Class<? extends WebDriver> driverClass) {
        super(name, testCase, aType, driverClass);
    }

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> data() {
        return getTests("/search/seznam/seznam-slovnik.krypton.html");
    }

    @Override
    protected String getBaseURL() {
        return "http://www.seznam.cz";
    }


}
