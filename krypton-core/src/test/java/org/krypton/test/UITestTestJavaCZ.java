package org.krypton.test;

import org.junit.Test;
import org.krypton.junit.SeleniumTestType;
import org.junit.runners.Parameterized;
import org.krypton.junit.UITestParent;
import org.krypton.support.KryptonTestExecutor;
import org.openqa.selenium.WebDriver;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 22.06.13
 * Time: 14:52
 * To change this template use File | Settings | File Templates.
 */
//@Ignore
public class UITestTestJavaCZ extends UITestParent {


    public UITestTestJavaCZ(String name, String testCase, SeleniumTestType aType, Class<? extends WebDriver> driverClass) {
        super(name, testCase, aType, driverClass);
    }

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> data() {
        return getTests("/portals/java.cz/search-fail.krypton.test");
    }

    @Override
    protected String getBaseURL() {
        return "http://www.java.cz";
    }

    @Test(expected = Exception.class)
    public void testSelenium() throws Exception {
       super.testSelenium();
    }
}
