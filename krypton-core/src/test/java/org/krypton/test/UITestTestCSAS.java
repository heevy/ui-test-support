package org.krypton.test;

import org.junit.Ignore;
import org.junit.runners.Parameterized;
import org.krypton.junit.SeleniumTestType;
import org.krypton.junit.UITestParent;
import org.openqa.selenium.WebDriver;

import java.util.Collection;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 22.06.13
 * Time: 14:52
 * To change this template use File | Settings | File Templates.
 */
@Ignore
public class UITestTestCSAS extends UITestParent {


    public UITestTestCSAS(String name, String testCase, SeleniumTestType aType, Class<? extends WebDriver> driverClass) {
        super(name, testCase, aType, driverClass);
    }

    @Parameterized.Parameters(name = "{0}")
    public static Collection<Object[]> data() {
        return getTests("/banks/csas/currencyList.krypton.html");
    }

    @Override
    protected String getBaseURL() {
        return "http://www.csas.cz";
    }


}
