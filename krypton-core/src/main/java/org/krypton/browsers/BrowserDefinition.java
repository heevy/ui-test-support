package org.krypton.browsers;

import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 24.07.13
 * Time: 19:19
 * To change this template use File | Settings | File Templates.
 */
public interface BrowserDefinition {
    DesiredCapabilities getCapabilities();
}
