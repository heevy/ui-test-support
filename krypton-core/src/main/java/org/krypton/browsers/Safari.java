package org.krypton.browsers;

import org.openqa.selenium.Platform;
import org.openqa.selenium.remote.DesiredCapabilities;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 24.07.13
 * Time: 19:20
 * To change this template use File | Settings | File Templates.
 */
public class Safari implements BrowserDefinition{

    @Override
    public DesiredCapabilities getCapabilities() {

        DesiredCapabilities dc =  DesiredCapabilities.safari();
        dc.setPlatform(Platform.MAC);
        return dc;

    }
}
