package org.krypton.browsers;

import org.krypton.browsers.phantomjs.PhantomJSProvider;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * Created by jdk on 24.03.14.
 */
public class PhantomJSDriver extends org.openqa.selenium.phantomjs.PhantomJSDriver {

    private static PhantomJSProvider provider = new PhantomJSProvider();
    private static Path tmpPath;

    static {
        try {
            tmpPath = Files.createTempDirectory("phantom");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public PhantomJSDriver() {
        super(getDesiredCapabilities(null));
    }

    private static DesiredCapabilities getDesiredCapabilities(DesiredCapabilities cap){
        DesiredCapabilities capabilities = cap;
        if(capabilities == null){
            capabilities = new DesiredCapabilities();
        }
        capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,provider.getPhantomJS(tmpPath).toString());
        return capabilities;
    }


}
