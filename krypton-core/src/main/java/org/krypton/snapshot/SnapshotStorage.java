package org.krypton.snapshot;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 16.07.13
 * Time: 18:12
 * To change this template use File | Settings | File Templates.
 */
public class SnapshotStorage {

    private String source;

    private String screenshot;

    //for serialization
    protected SnapshotStorage() {
    }

    public SnapshotStorage(String source, String screenshot) {
        this.source = source;
        this.screenshot = screenshot;
    }

    public String getSource() {
        return source;
    }

    public String getScreenshot() {
        return screenshot;
    }
}
