package org.krypton.snapshot;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.TemplateExceptionHandler;
import org.apache.commons.io.FileUtils;
import org.krypton.core.executors.parser.SeleniumTest;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.krypton.core.SeleniumExecutorContext;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.spi.CommandExecutorListener;
import org.krypton.core.spi.TestExecutorListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;

/**
 * Takes snapshot after each command. Snapshots consists of HTML and screenshot.
 */
@Component
public class TakeSnapshotListener implements CommandExecutorListener, TestExecutorListener {

	public static final Logger LOG = LoggerFactory.getLogger(TakeSnapshotListener.class);

	private static Configuration cfg;

	// TODO Joudek Use spring for it or put it to better place
	static {
		/* Create and adjust the configuration */
		cfg = new Configuration();

		cfg.setClassForTemplateLoading(TakeSnapshotListener.class, "/");

		cfg.setObjectWrapper(new DefaultObjectWrapper());
		cfg.setDefaultEncoding("UTF-8");
		cfg.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
	}

	@Override
	public void commandExecuted(EvaluatedCommand command, int aCommandIndex, SeleniumExecutorContext ctx, SeleniumTest aTest) {
		String snapshotID = Integer.toString(aCommandIndex);
		File screenshot = null;
		File source = null;
		if (ctx.getDriver() instanceof TakesScreenshot) {
			if (ctx.getFileStorage() == null) {
				return;
			}
			LOG.info("Taking snapshot with ID '{}' into directory '{}'.",snapshotID,ctx.getFileStorage().getAbsolutePath());
			source = new File(ctx.getFileStorage(),snapshotID + ".html");
			try {
				FileUtils.write(source, ctx.getDriver().getPageSource());
			} catch (IOException e) {
				throw new RuntimeException("Cannot write html source.", e);
			}
			byte[] scr = ((TakesScreenshot) ctx.getDriver()).getScreenshotAs(OutputType.BYTES);
			screenshot = new File(ctx.getFileStorage(),snapshotID +  ".png");
			try {
				FileUtils.writeByteArrayToFile(screenshot, scr);
			} catch (IOException e) {
				throw new RuntimeException("Cannot write screenshot.", e);
			}
            SnapshotStorage storage = new SnapshotStorage(source.getName(),screenshot.getName());
            ctx.getResults().addCommandStorage(SnapshotStorage.class.getSimpleName(),storage);
		}
	}



	@Override
	public int getOrder() {
		return 200;
	}

	@Override
	public void beforeTestExecution(SeleniumTest aTest, SeleniumExecutorContext aCtx) {

	}

	@Override
	public void testExecuted(SeleniumTest aTest, SeleniumExecutorContext aCtx) {
		testExceutedWithError(aTest, aCtx, null);
	}


	@Override
	public void testExceutedWithError(SeleniumTest aTest, SeleniumExecutorContext aCtx, Throwable aError) {
//		// TODO Joudek refactor
//		String outputFolder = System.getProperty(RESULT_DIR_PROPERTY);
//		if (outputFolder == null) {
//			return;
//		}
//		/* Create a data-model */
//		Map root = new HashMap();
//		root.put("testCase", aTest);
//		List<CommandStorage> storage = getStorage(aCtx);
//		List<Command> commands = aTest.convertCommands();
//		for (int i = storage.size(); i < commands.size(); i++) {
//			storage.add(new CommandStorage(commands.get(i), false));
//		}
//
//		BeansWrapper bw = BeansWrapper.getDefaultInstance();
//		try {
//			root.put("commandsStorage", bw.wrap(getStorage(aCtx)));
//		} catch (TemplateModelException e) {
//			throw new RuntimeException(e);
//		}
//
//		/* Get the template */
//		Template temp = null;
//		try {
//			temp = cfg.getTemplate("/org/krypton/templates/testCase.ftl");
//		} catch (IOException e) {
//			throw new RuntimeException("Cannot load transformation template.", e);
//		}
//
//		/* Merge data-model with template */
//		StringWriter out = new StringWriter();
//		try {
//			temp.process(root, out);
//		} catch (TemplateException e) {
//			throw new RuntimeException("Cannot process template.", e);
//		} catch (IOException e) {
//			throw new RuntimeException("Cannot process template.", e);
//		}
//		try {
//			FileUtils.write(new File(concatFilename(getTestFolder(outputFolder, aTest, aCtx), "result.html")), out.toString());
//		} catch (IOException e) {
//			throw new RuntimeException("Cannot write ui test result report.", e);
//		}
	}

}
