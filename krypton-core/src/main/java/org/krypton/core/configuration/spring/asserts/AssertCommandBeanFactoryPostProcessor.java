package org.krypton.core.configuration.spring.asserts;

import org.krypton.core.spi.AssertCommandExecutor;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;

/**
 * Bean post processor which creates command executors for :
 * 
 * <pre>
 * verify[command]
 * verifyNot[command]
 * assert[command]
 * assertNot[command]
 * 
 * <pre/>
 */
public class AssertCommandBeanFactoryPostProcessor implements BeanDefinitionRegistryPostProcessor {

	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		for (String definitionName : registry.getBeanDefinitionNames()) {
			BeanDefinition def = registry.getBeanDefinition(definitionName);
			Class clazz;
			try {
				clazz = Class.forName(def.getBeanClassName());
			} catch (ClassNotFoundException e) {
				continue;
			}
			if (AssertCommandExecutor.class.isAssignableFrom(clazz)) {
				String seleniumCommand = definitionName.substring(AssertCommandExecutor.ASSERT_BEAN_BASE_NAME.length());
				if (seleniumCommand.endsWith("Present")) {
                    seleniumCommand=seleniumCommand.substring(0,seleniumCommand.length()-"Present".length());
                    createBean(registry, def, AssertCommandExecutor.BEAN_BASE_NAME + "verify" + seleniumCommand+"Present", false, false);
                    createBean(registry, def, AssertCommandExecutor.BEAN_BASE_NAME + "verify" + seleniumCommand+"NotPresent", false, true);
                    createBean(registry, def, AssertCommandExecutor.BEAN_BASE_NAME + "assert" + seleniumCommand+"Present", true, false);
                    createBean(registry, def, AssertCommandExecutor.BEAN_BASE_NAME + "assert" + seleniumCommand+"NotPresent", true, true);
				} else {
					createBean(registry, def, AssertCommandExecutor.BEAN_BASE_NAME + "verify" + seleniumCommand, false, false);
					createBean(registry, def, AssertCommandExecutor.BEAN_BASE_NAME + "verifyNot" + seleniumCommand, false, true);
					createBean(registry, def, AssertCommandExecutor.BEAN_BASE_NAME + "assert" + seleniumCommand, true, false);
					createBean(registry, def, AssertCommandExecutor.BEAN_BASE_NAME + "assertNot" + seleniumCommand, true, true);
				}
			}
		}
	}

	private void createBean(BeanDefinitionRegistry beanDefinitionRegistry, BeanDefinition parentBeanDefinition, String beanName, boolean strict,
			boolean invertAssert) {
		if (parentBeanDefinition instanceof AbstractBeanDefinition) {
			AbstractBeanDefinition newDefinition = ((AbstractBeanDefinition) parentBeanDefinition).cloneBeanDefinition();
			newDefinition.getPropertyValues().addPropertyValue("strict", strict);
			newDefinition.getPropertyValues().addPropertyValue("invertAssert", invertAssert);
			beanDefinitionRegistry.registerBeanDefinition(beanName, newDefinition);
		} else {
			throw new RuntimeException("Cannot clone bean definition " + parentBeanDefinition.getBeanClassName());
		}

	}

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {

	}
}
