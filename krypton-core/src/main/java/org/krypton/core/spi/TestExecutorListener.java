package org.krypton.core.spi;

import org.krypton.core.SeleniumExecutorContext;
import org.krypton.core.executors.parser.SeleniumTest;
import org.krypton.core.executors.parser.SeleniumTestCase;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 06.07.13
 * Time: 17:52
 * To change this template use File | Settings | File Templates.
 */
public interface TestExecutorListener {
    void beforeTestExecution(SeleniumTest aTest, SeleniumExecutorContext aCtx);

    void testExecuted(SeleniumTest aTest, SeleniumExecutorContext aCtx);

    void testExceutedWithError(SeleniumTest aTest, SeleniumExecutorContext aCtx, Throwable aError);
}
