package org.krypton.core.spi;

import org.krypton.core.executors.parser.SeleniumTest;
import org.springframework.core.Ordered;

import org.krypton.core.SeleniumExecutorContext;
import org.krypton.core.executors.parser.SeleniumTestCase;
import org.krypton.core.executors.support.EvaluatedCommand;

/**
 * Listeners of Selenium commands test executor. It is executed synchronously after each command. It can be used fo waiting for ajax or taking screenshots.
 *
 */
public interface CommandExecutorListener extends Ordered {

    /**
     * Callback method which is executed after each command.
     *
     * @param command command to execute
     * @param aCommandIndex index of command in test
     * @param ctx test execution context
     * @param aTest test case which has been executed
     */
	void commandExecuted(EvaluatedCommand command, int aCommandIndex, SeleniumExecutorContext ctx, SeleniumTest aTest);
}
