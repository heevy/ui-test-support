package org.krypton.core.spi;

import org.junit.Assert;

/**
 * Generic parent for all verify/assert selenium commands.
 */
public abstract class AssertCommandExecutor implements CommandExecutor {

	public static final String ASSERT_BEAN_BASE_NAME = "selenium-assert-executor-";

	private boolean strict;

	private boolean invertAssert;

	/**
	 * Just for default bean creation.
	 */
	protected AssertCommandExecutor() {

	}

	public boolean isStrict() {
		return strict;
	}

	public boolean isInvertAssert() {
		return invertAssert;
	}

	public void setStrict(boolean strict) {
		this.strict = strict;
	}

	public void setInvertAssert(boolean invertAssert) {
		this.invertAssert = invertAssert;
	}

	protected void assertEquals(Object aExpected, Object aResult) {
        if (!isInvertAssert()) {
			Assert.assertEquals(aExpected, aResult);
		} else {
			Assert.assertNotEquals(aExpected, aResult);
		}
	}

    protected void assertNotNull(Object aResult) {
        if (!isInvertAssert()) {
            Assert.assertNotNull(aResult);
        } else {
            Assert.assertNull(aResult);
        }
    }

}
