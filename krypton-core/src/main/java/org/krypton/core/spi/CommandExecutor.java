package org.krypton.core.spi;

import org.krypton.core.SeleniumExecutorContext;
import org.krypton.core.executors.support.EvaluatedCommand;

/**
 * Interface for Selenium Selenese command executor.
 */
public interface CommandExecutor {

    public static final String BEAN_BASE_NAME="selenium-executor-";

    /**
     * Evaluates command.
     * @param aCommand command to evaluate
     * @param ctx test executor context
     */
	void execute(EvaluatedCommand aCommand, SeleniumExecutorContext ctx);
}
