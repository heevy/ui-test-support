package org.krypton.core;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.krypton.storage.TestResults;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

/**
 * Selenium execution context. Contains all shared states as variables and webdriver.
 */
public class SeleniumExecutorContext {
	private Map<String, Object> variables = new HashMap<>();

	private WebDriver driver;

	private JavascriptExecutor javascript;

	private String baseURL;

	private TestResults results;

	private File fileStorage;

	private int timeout = 10000;

	public SeleniumExecutorContext(WebDriver driver, String aBaseURL, TestResults result) {
		this.driver = driver;
		this.baseURL = aBaseURL;
		this.javascript = (JavascriptExecutor) driver;
		this.results = result;
	}

	public File getFileStorage() {
		return fileStorage;
	}

	public void setFileStorage(File fileStorage) {
		this.fileStorage = fileStorage;
	}

	public Map<String, Object> getVariables() {
		return variables;
	}

	public WebDriver getDriver() {
		return driver;
	}

	public JavascriptExecutor getJavascript() {
		return javascript;
	}

	public String getBaseURL() {
		return baseURL;
	}

	public int getTimeout() {
		return timeout;
	}

	public TestResults getResults() {
		return results;
	}

	public String getLocationURL(String aRelative) {
		return getBaseURL() + aRelative;
	}

	private static final Pattern PROPERTY_REGEXP = Pattern.compile("\\$\\{(.+?)\\}");

	public String replaceProperties(final String value) {
		String res = value;
		Matcher m = PROPERTY_REGEXP.matcher(res);
		while (m.find()) {
			Object propertyValue = variables.get(m.group(1));
			String propertyValueString = null;
			if (propertyValue != null) {
				propertyValueString = propertyValue.toString();
			}
			res = res.replace("${" + m.group(1) + "}", propertyValueString);
			m = PROPERTY_REGEXP.matcher(res);
		}
		return res;
	}
}
