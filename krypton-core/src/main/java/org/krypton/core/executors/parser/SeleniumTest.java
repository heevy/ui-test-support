package org.krypton.core.executors.parser;

import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 17.07.13
 * Time: 13:48
 * To change this template use File | Settings | File Templates.
 */
public interface SeleniumTest {

    void setTitle(String title);

    String getTitle();

    public List<Command> convertCommands();
}
