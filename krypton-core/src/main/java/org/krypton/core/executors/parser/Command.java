package org.krypton.core.executors.parser;

/**
 * Description of Selenium selenese command which has been parsed from HTML test storage.
 */
public class Command {
	private String command;

	private String target;

	private String value;

	public Command(String command, String target, String value) {
		this.command = command;
		this.target = target;
		this.value = value;
	}

	public String getCommand() {
		return command;
	}

	public String getTarget() {
		return target;
	}

	public String getValue() {
		return value;
	}

	public String getType() {
		return "command";
	}

	@Override
	public String toString() {
		return "Command{" + "command='" + command + '\'' + ", target='" + target + '\'' + ", value='" + value + '\'' + '}';
	}
}
