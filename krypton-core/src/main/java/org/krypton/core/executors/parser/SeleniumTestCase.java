package org.krypton.core.executors.parser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 20.06.13
 * Time: 23:44
 * To change this template use File | Settings | File Templates.
 */
public class SeleniumTestCase implements SeleniumTest {

	private String header;
	private String footer;
	private String baseURL;
	private Object[] commands;
    private String title;

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	public String getBaseURL() {
		return baseURL;
	}

	public void setBaseURL(String baseURL) {
		this.baseURL = baseURL;
	}

	public Object[] getCommands() {
		return commands;
	}

	public void setCommands(Object[] commands) {
		this.commands = commands;
	}

	public List<Command> convertCommands() {
		List<Command> res = new ArrayList<>(commands.length);
		for (Object o : commands) {
			Map<String, String> jsObject = (Map) o;
			res.add(new Command(jsObject.get("command"), jsObject.get("target"), jsObject.get("value")));
		}
		return res;
	}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
	public String toString() {
		return "SeleniumTestCase{" + "header=" + header + ", footer=" + footer + ", baseURL=" + baseURL + ", commands=" + Arrays.toString(commands) + '}';
	}
}
