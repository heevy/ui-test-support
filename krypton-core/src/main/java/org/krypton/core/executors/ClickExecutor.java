package org.krypton.core.executors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.krypton.core.spi.CommandExecutor;
import org.krypton.core.executors.support.ElementsLocator;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.SeleniumExecutorContext;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 21.06.13
 * Time: 10:24
 * To change this template use File | Settings | File Templates.
 */
@Component("selenium-executor-click")
public class ClickExecutor implements CommandExecutor {

	@Autowired
	private ElementsLocator elementsLocator;

	@Override
	public void execute(EvaluatedCommand aCommand, SeleniumExecutorContext ctx) {
		elementsLocator.locate(aCommand.getTarget(), ctx).click();
	}
}
