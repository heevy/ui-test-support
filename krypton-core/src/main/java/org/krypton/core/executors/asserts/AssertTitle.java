package org.krypton.core.executors.asserts;

import org.krypton.core.spi.AssertCommandExecutor;
import org.krypton.core.spi.CommandExecutor;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.SeleniumExecutorContext;
import org.junit.Assert;
import org.springframework.stereotype.Component;

@Component(AssertCommandExecutor.ASSERT_BEAN_BASE_NAME+"Title")
public class AssertTitle extends AssertCommandExecutor {

    @Override
	public void execute(EvaluatedCommand aCommand, SeleniumExecutorContext ctx) {
		assertEquals(aCommand.getTarget(String.class), ctx.getDriver().getTitle());
	}
}
