package org.krypton.core.executors.support;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.krypton.core.SeleniumExecutorContext;
import org.springframework.stereotype.Component;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 21.06.13
 * Time: 10:55
 * To change this template use File | Settings | File Templates.
 */
@Component
public class ElementsLocator {
	private static final Pattern LOC_PATTERN = Pattern.compile("([a-zA-Z0-9]+)=(.+)");

	public By getLocator(final String aLocationString) {
		Matcher m = LOC_PATTERN.matcher(aLocationString);
		if (m.matches()) {
			switch (m.group(1)) {
			case "id":
				return By.id(m.group(2));
			case "css":
				return By.cssSelector(m.group(2));
			case "name":
				return By.name(m.group(2));
			case "link":
				return By.linkText(m.group(2));
            case "xpath":
                return  By.xpath(m.group(2));
			}
			throw new UnsupportedOperationException("Locator '" + m.group(1) + "' is not supported.");
		} else {
			return By.xpath(aLocationString);
		}

	}

	public WebElement locate(Object location, SeleniumExecutorContext ctx) {
		if (location instanceof WebElement) {
			return (WebElement) location;
		} else {
			return ctx.getDriver().findElement(getLocator(location.toString()));
		}
	}
}
