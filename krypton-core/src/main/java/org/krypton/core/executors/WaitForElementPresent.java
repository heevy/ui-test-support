package org.krypton.core.executors;

import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.krypton.core.spi.CommandExecutor;
import org.krypton.core.executors.support.ElementsLocator;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.SeleniumExecutorContext;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 21.06.13
 * Time: 10:24
 * To change this template use File | Settings | File Templates.
 */
@Component("selenium-executor-waitForElementPresent")
public class WaitForElementPresent implements CommandExecutor {

	@Autowired
	private ElementsLocator elementsLocator;

	@Override
	public void execute(EvaluatedCommand aCommand, SeleniumExecutorContext ctx) {
		int time = 0;
		while (elementsLocator.locate(aCommand.getTarget(), ctx) == null) {
			if (time >= ctx.getTimeout()) {
				Assert.fail("Element '" + aCommand.getTarget() + "' did not appear in " + ctx.getTimeout() + " ms.");
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			time = time + 100;
		}
	}
}
