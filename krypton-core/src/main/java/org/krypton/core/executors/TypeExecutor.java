package org.krypton.core.executors;

import org.krypton.core.spi.CommandExecutor;
import org.krypton.core.executors.support.ElementsLocator;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.SeleniumExecutorContext;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 21.06.13
 * Time: 10:24
 * To change this template use File | Settings | File Templates.
 */
@Component("selenium-executor-type")
public class TypeExecutor implements CommandExecutor {

	@Autowired
	private ElementsLocator elementsLocator;

	@Override
	public void execute(EvaluatedCommand aCommand, SeleniumExecutorContext ctx) {
		WebElement element = elementsLocator.locate(aCommand.getTarget(), ctx);
		element.clear();
		element.sendKeys(aCommand.getValue(String.class));
	}
}
