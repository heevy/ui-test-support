package org.krypton.core.executors.asserts;

import org.krypton.core.spi.AssertCommandExecutor;
import org.krypton.core.spi.CommandExecutor;
import org.krypton.core.executors.support.ElementsLocator;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.SeleniumExecutorContext;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 21.06.13
 * Time: 10:24
 * To change this template use File | Settings | File Templates.
 */
@Component(AssertCommandExecutor.ASSERT_BEAN_BASE_NAME+"Text")
public class AssertText extends AssertCommandExecutor {
	@Autowired
	private ElementsLocator elementsLocator;

	@Override
	public void execute(EvaluatedCommand aCommand, SeleniumExecutorContext ctx) {
		Pattern p = Pattern.compile("^[\\s\\S]*" + aCommand.getTarget(String.class) + "[\\s\\S]*$");
		assertEquals(aCommand.getValue(String.class), elementsLocator.locate(aCommand.getTarget(String.class), ctx).getText());
	}
}
