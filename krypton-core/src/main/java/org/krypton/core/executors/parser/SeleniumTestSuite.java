package org.krypton.core.executors.parser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 17.07.13
 * Time: 11:10
 * To change this template use File | Settings | File Templates.
 */
public class SeleniumTestSuite implements SeleniumTest {

    private String title;

    private List<SeleniumTest> tests=new ArrayList<>();

    public String getTitle() {
        return title;
    }

    @Override
    public List<Command> convertCommands() {
        List<Command> commands = new ArrayList<>();
        for(SeleniumTest testCase:tests){
            commands.addAll(testCase.convertCommands());
        }
        return commands;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<SeleniumTest> getTests() {
        return tests;
    }
}
