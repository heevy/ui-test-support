package org.krypton.core.executors.support;

import org.krypton.core.executors.parser.Command;
import org.krypton.core.SeleniumExecutorContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 21.06.13
 * Time: 13:37
 * To change this template use File | Settings | File Templates.
 */
public class EvaluatedCommand {

	public static final Logger LOG = LoggerFactory.getLogger(EvaluatedCommand.class);

	private Command command;

	private SeleniumExecutorContext seleniumExecutorContext;

	private static final Pattern JAVASCRIPT_RE = Pattern.compile("javascript\\{(.+)\\}");
	private static final Pattern PROPERTY_RE = Pattern.compile("\\$\\{([a-zA-Z_0-9]+)\\}");

	public EvaluatedCommand(Command command, SeleniumExecutorContext seleniumExecutorContext) {
		this.command = command;
		this.seleniumExecutorContext = seleniumExecutorContext;
	}

	public Object getTarget() {
		String expression = command.getTarget();
		return evaluate(expression);
	}

	public Object getValue() {
		String expression = command.getValue();
		return evaluate(expression);
	}

	public <T> T getValue(Class<T> resultClass) {
		return (T) getValue();
	}

	public <T> T getTarget(Class<T> resultClass) {
		return (T) getTarget();
	}

	public Command getCommand() {
		return command;
	}

	private Object evaluate(String expression) {
		Matcher m = JAVASCRIPT_RE.matcher(expression);
		if (m.matches()) {
			return seleniumExecutorContext.getJavascript().executeScript("return " + m.group(1));
		}
		m = PROPERTY_RE.matcher(expression);
		if (m.matches()) {
			LOG.info("Replace '{}' by '{}'.", expression, seleniumExecutorContext.getVariables().get(m.group(1)));
			return seleniumExecutorContext.getVariables().get(m.group(1));
		}
		return seleniumExecutorContext.replaceProperties(expression);
	}
}
