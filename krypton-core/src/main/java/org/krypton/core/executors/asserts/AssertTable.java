package org.krypton.core.executors.asserts;

import org.junit.Assert;
import org.krypton.core.spi.AssertCommandExecutor;
import org.openqa.selenium.WebElement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import org.krypton.core.spi.CommandExecutor;
import org.krypton.core.executors.support.ElementsLocator;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.SeleniumExecutorContext;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 21.06.13
 * Time: 10:24
 * To change this template use File | Settings | File Templates.
 */
@Component(AssertCommandExecutor.ASSERT_BEAN_BASE_NAME+"Table")
public class AssertTable extends AssertCommandExecutor {

	@Autowired
	private ElementsLocator elementsLocator;

	@Override
	public void execute(EvaluatedCommand aCommand, SeleniumExecutorContext ctx) {
		WebElement el = elementsLocator.locate(convertTableToXpath(aCommand.getTarget(String.class)), ctx);
		assertEquals(aCommand.getValue(String.class), el.getText());
	}

	private String convertTableToXpath(String tableLocator) {
		String[] parts = tableLocator.split("\\.");
		return "//table[@id='" + parts[0] + "']/tbody/tr[" + (Integer.valueOf(parts[1]) ) + "]//td[" + (Integer.valueOf(parts[2])+1) + "]";
	}
}
