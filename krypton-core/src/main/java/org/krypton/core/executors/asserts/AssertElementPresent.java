package org.krypton.core.executors.asserts;

import org.krypton.core.spi.AssertCommandExecutor;
import org.krypton.core.spi.CommandExecutor;
import org.krypton.core.executors.support.ElementsLocator;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.SeleniumExecutorContext;
import org.junit.Assert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 21.06.13
 * Time: 10:24
 * To change this template use File | Settings | File Templates.
 */
@Component(AssertCommandExecutor.ASSERT_BEAN_BASE_NAME+"ElementPresent")
public class AssertElementPresent extends AssertCommandExecutor {

	@Autowired
	private ElementsLocator elementsLocator;

	@Override
	public void execute(EvaluatedCommand aCommand, SeleniumExecutorContext ctx) {
		assertNotNull(elementsLocator.locate(aCommand.getTarget(), ctx));

	}
}
