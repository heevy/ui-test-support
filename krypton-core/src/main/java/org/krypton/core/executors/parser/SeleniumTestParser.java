package org.krypton.core.executors.parser;

import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import javax.script.Invocable;
import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 22.06.13
 * Time: 14:38
 * To change this template use File | Settings | File Templates.
 */
@Component
public class SeleniumTestParser {
	public static final Logger LOG = LoggerFactory.getLogger(SeleniumTestParser.class);

	public SeleniumTestCase parseTest(Resource aSource) {
		// create a script engine manager
		ScriptEngineManager factory = new ScriptEngineManager();
		// create JavaScript engine
		ScriptEngine engine = factory.getEngineByName("JavaScript");

        engine.put("log", LOG);
		// evaluate JavaScript code from given file - specified by first argument
		eval(engine,new ClassPathResource("/org/krypton/parser/selenium4j.js"));
		eval(engine, new ClassPathResource("/org/krypton/parser/xhtml-entities.js"));
        eval(engine,new ClassPathResource("/org/krypton/parser/seleniumHtmlParser.js"));

		Invocable inv = (Invocable) engine;

        String content = null;
        try {
            content = IOUtils.toString(aSource.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException("Cannot read test from resource "+aSource,e);
        }
        SeleniumTestCase tc = new SeleniumTestCase();
        try {
            inv.invokeFunction("parse", tc, content);
        } catch (ScriptException e) {
            throw new RuntimeException("Cannot parse test from resource "+aSource,e);
        } catch (NoSuchMethodException e) {
            throw new RuntimeException("Cannot parse test from resource "+aSource,e);
        }
        return tc;
	}

	private void eval(ScriptEngine engine, Resource aRes) {
        InputStreamReader isr=null;
		try {
             isr= new InputStreamReader((aRes.getInputStream()));
			engine.eval(isr);
		} catch (ScriptException e) {
            throw new RuntimeException("Cannot evaluate resource "+ aRes,e);
        } catch (IOException e) {
            throw new RuntimeException("Cannot load resource "+ aRes,e);
        } finally {
			IOUtils.closeQuietly(isr);
		}

	}

    public static final Pattern TEST_SUITE_TABLE_PATTERN=Pattern.compile("(<table[\\s>][\\s\\S]*?</table>)");
    public static final Pattern TEST_CASE_ROW_PATTERN=Pattern.compile("(<tr[\\s>][\\s\\S]*?</tr>)");
    public static final Pattern TEST_CASE_LINK_PATTERN=Pattern.compile("<a\\s[^>]*href=['\"]([^'\"]+)['\"][^>]*>([\\s\\S]+)</a>");

    public SeleniumTestSuite parseTestSuite(Resource aSource) {
        String testSuiteHtml;
        try {
            testSuiteHtml = IOUtils.toString(aSource.getInputStream());
        } catch (IOException e) {
            throw new RuntimeException("Cannot read test suite "+aSource,e);
        }
        Matcher m =TEST_SUITE_TABLE_PATTERN.matcher(testSuiteHtml);

        //TODO refactor to support non file resources
        File resDir;
        try {
            File resFile = aSource.getFile();
            resDir = resFile.getParentFile();
        } catch (IOException e) {
            throw new RuntimeException("Cannot get file from resource : " + aSource,e);
        }
        String resDirPath = resDir.getAbsolutePath();

        SeleniumTestSuite suite = new SeleniumTestSuite();
        if(m.find()){
            String tableHtml = m.group(1);
            m = TEST_CASE_ROW_PATTERN.matcher(tableHtml);
            while(m.find()){
                String tableRow = m.group(1);
                Matcher linkMather = TEST_CASE_LINK_PATTERN.matcher(tableRow);
                if(linkMather.find()){
                    String resourceRelativePath = linkMather.group(1);
                    resourceRelativePath=resourceRelativePath.replace("/",File.separator);
                    String testCasePath = FilenameUtils.normalize(resDirPath+File.separator+resourceRelativePath);
                    SeleniumTestCase testCase = parseTest(new FileSystemResource(testCasePath));
                    suite.getTests().add(testCase);
                }
            }
        }
        return suite;
    }
}
