package org.krypton.core.executors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import org.krypton.core.spi.CommandExecutor;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.SeleniumExecutorContext;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 21.06.13
 * Time: 10:09
 * To change this template use File | Settings | File Templates.
 */
@Component("selenium-executor-storeEval")
public class StoreEvalExecutor implements CommandExecutor {
    public static final Logger LOG = LoggerFactory.getLogger(StoreEvalExecutor.class);

	@Override
	public void execute(EvaluatedCommand aCommand, SeleniumExecutorContext ctx) {
		Object o = ctx.getJavascript().executeScript("return " + aCommand.getTarget());
		LOG.info("Variable '{}' has value '{}'.",aCommand.getValue(),o);
        ctx.getVariables().put(aCommand.getValue(String.class), o);
	}
}
