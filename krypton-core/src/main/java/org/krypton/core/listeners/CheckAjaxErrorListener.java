package org.krypton.core.listeners;

import java.util.List;

import org.junit.Assert;
import org.krypton.core.SeleniumExecutorContext;
import org.krypton.core.executors.parser.SeleniumTest;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.spi.CommandExecutorListener;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Check if there is AJAX error during test.
 */
@Component
public class CheckAjaxErrorListener implements CommandExecutorListener {
	public static final Logger LOG = LoggerFactory.getLogger(CheckAjaxErrorListener.class);

	@Autowired
	private WaitForAjaxListener waitForAjaxListener;

	@Override
	public void commandExecuted(EvaluatedCommand command, int aCommandIndex, SeleniumExecutorContext ctx, SeleniumTest aTest) {
		List<WebElement> errorElements = ctx.getDriver().findElements(By.id("fatalErrorDialog"));
		if (errorElements.size() > 0) {
			Assert.fail("Error has been found. Please check the result.");
		}
		long ajaxTasks = (Long) ctx.getJavascript().executeScript("if(typeof(jQuery)=='undefined'){return 0}else{return jQuery.active}");
		if (ajaxTasks != 0) {
			//TODO hack beacuse of ajax in EMFIP. Need proper design.
            waitForAjaxListener.commandExecuted(command, aCommandIndex, ctx, aTest);
            ajaxTasks = (Long) ctx.getJavascript().executeScript("if(typeof(jQuery)=='undefined'){return 0}else{return jQuery.active}");
            //hack end

			if (ajaxTasks != 0) {
				Assert.fail("Ajax error has been found. There are " + ajaxTasks
						+ " ajax tasks which has not been processed or ended by javascript error. Please check Javascript console. ");
			}
		}
	}

	@Override
	public int getOrder() {
		return 20000;
	}
}
