package org.krypton.core.listeners;

import org.krypton.core.executors.parser.SeleniumTest;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.SeleniumExecutorContext;
import org.krypton.core.spi.CommandExecutorListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Check if jQuery ajax has been completed.
 */
@Component
public class WaitForAjaxListener implements CommandExecutorListener{
    public static final Logger LOG = LoggerFactory.getLogger(WaitForAjaxListener.class);

    @Override
    public void commandExecuted(EvaluatedCommand command, int aCommandIndex, SeleniumExecutorContext ctx, SeleniumTest aTest) {
        LOG.info("Wait for ajax.");
        long ajaxTasks=0;
        int time=0;
        while((ajaxTasks = (Long) ctx.getJavascript().executeScript("if(typeof(jQuery)=='undefined'){return 0}else{return jQuery.active}"))!=0){
            if (time >= ctx.getTimeout()) {
                LOG.error("Ajax has not been completed in " + ctx.getTimeout() + " ms.");
                return;
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            time+=100;
        }
    }

    @Override
    public int getOrder() {
        return 100;
    }
}
