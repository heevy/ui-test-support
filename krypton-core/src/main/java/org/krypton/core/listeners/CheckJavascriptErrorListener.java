package org.krypton.core.listeners;

import org.krypton.core.SeleniumExecutorContext;
import org.krypton.core.executors.parser.SeleniumTest;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.spi.CommandExecutorListener;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebElement;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Check if there is AJAX error during test.
 */
@Component
public class CheckJavascriptErrorListener implements CommandExecutorListener {
	public static final Logger LOG = LoggerFactory.getLogger(CheckJavascriptErrorListener.class);

	@Override
	public void commandExecuted(EvaluatedCommand command, int aCommandIndex, SeleniumExecutorContext ctx, SeleniumTest aTest) {
		try {
			WebElement body = ctx.getDriver().findElement(By.tagName("body"));
			if (body.getAttribute("jserror") != null) {
				LOG.error("There has been found Javascript error :" + body.getAttribute("jserror"));
			}
		} catch (StaleElementReferenceException e) {
			LOG.error("Cannot check for javascript errors.", e);
		}
	}

	@Override
	public int getOrder() {
		return 10000;
	}
}
