package org.krypton.core.listeners;

import org.krypton.core.SeleniumExecutorContext;
import org.krypton.core.executors.parser.SeleniumTest;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.spi.CommandExecutorListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Wait for defined amount of time.
 */
@Component
public class WaitListener implements CommandExecutorListener{
    public static final Logger LOG = LoggerFactory.getLogger(WaitListener.class);

    private int timeout=500;

    @Override
    public void commandExecuted(EvaluatedCommand command, int aCommandIndex, SeleniumExecutorContext ctx, SeleniumTest aTest) {
        LOG.info("Wait for {}ms",timeout);
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getOrder() {
        return 10;
    }
}
