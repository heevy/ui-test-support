package org.krypton.core.listeners;

import org.krypton.core.SeleniumExecutorContext;
import org.krypton.core.executors.parser.SeleniumTest;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.spi.CommandExecutorListener;
import org.krypton.core.spi.TestExecutorListener;
import org.krypton.storage.TestEnvironment;
import org.krypton.storage.TestResult;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.stereotype.Component;

/**
 * Check if jQuery ajax has been completed.
 */
@Component
public class TestStorageListener implements CommandExecutorListener, TestExecutorListener {

	@Override
	public void commandExecuted(EvaluatedCommand command, int aCommandIndex, SeleniumExecutorContext ctx, SeleniumTest aTest) {
		ctx.getResults().addCommand(command.getCommand());
	}

	@Override
	public int getOrder() {
		return 10;
	}

	@Override
	public void beforeTestExecution(SeleniumTest aTest, SeleniumExecutorContext aCtx) {
		if (aCtx.getDriver() instanceof RemoteWebDriver) {
			String browserName = ((RemoteWebDriver) aCtx.getDriver()).getCapabilities().getBrowserName();
			String browserVersion = ((RemoteWebDriver) aCtx.getDriver()).getCapabilities().getVersion();
			String browserPlatform = ((RemoteWebDriver) aCtx.getDriver()).getCapabilities().getPlatform().toString();
            TestEnvironment env = new TestEnvironment(browserName, browserVersion, browserPlatform);
            aCtx.getResults().addTestStorage(TestEnvironment.class.getSimpleName(),env);
		}
	}

	@Override
	public void testExecuted(SeleniumTest aTest, SeleniumExecutorContext aCtx) {
		TestResult res = new TestResult(true);
		aCtx.getResults().addTestStorage(TestResult.class.getSimpleName(), res);
	}

	@Override
	public void testExceutedWithError(SeleniumTest aTest, SeleniumExecutorContext aCtx, Throwable aError) {
		TestResult res = new TestResult(false, aError);
		aCtx.getResults().addTestStorage(TestResult.class.getSimpleName(), res);
	}
}
