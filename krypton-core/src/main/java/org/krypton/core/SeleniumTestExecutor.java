package org.krypton.core;

import org.krypton.core.executors.parser.Command;
import org.krypton.core.executors.parser.SeleniumTest;
import org.krypton.core.executors.parser.SeleniumTestCase;
import org.krypton.core.executors.parser.SeleniumTestParser;
import org.krypton.core.executors.parser.SeleniumTestSuite;
import org.krypton.core.executors.support.EvaluatedCommand;
import org.krypton.core.spi.CommandExecutor;
import org.krypton.core.spi.CommandExecutorListener;
import org.krypton.core.spi.TestExecutorListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.OrderComparator;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;

/**
 * Selenium test executor.
 */
@Component
public class SeleniumTestExecutor {

	@Autowired
	private BeanFactory beanFactory;

	@Autowired
	private SeleniumTestParser seleniumTestParser;

	private List<CommandExecutorListener> listeners;

	private List<TestExecutorListener> testListeners;

	public static final Logger LOG = LoggerFactory.getLogger(SeleniumTestExecutor.class);

	/**
	 * Parses and executes Selenium test case from HTML format.
	 * 
	 * @param aTest
	 *            test source
	 * @param ctx
	 *            test execution context
	 */
	public void executeTest(Resource aTest, SeleniumExecutorContext ctx) {
		executeTest(seleniumTestParser.parseTest(aTest), ctx);
	}

	/**
	 * Executes Selenium test case from parsed test.
	 * 
	 * @param tc
	 *            selenium test case
	 * @param ctx
	 *            test execution context
	 */
	public void executeTest(SeleniumTest tc, SeleniumExecutorContext ctx) {
		int i = 0;
		for (TestExecutorListener listener : testListeners) {
			listener.beforeTestExecution(tc, ctx);
		}
		Throwable error = null;
		try {
			for (Command c : tc.convertCommands()) {
				i++;
				LOG.info("Executing command : {}", c);
				CommandExecutor executor = beanFactory.getBean("selenium-executor-" + c.getCommand(), CommandExecutor.class);
				EvaluatedCommand eCommand = new EvaluatedCommand(c, ctx);
				executor.execute(eCommand, ctx);
				for (CommandExecutorListener listener : listeners) {
					listener.commandExecuted(eCommand, i, ctx, tc);
				}
            }
		} catch (Throwable e) {
			error = e;
			throw e;
		} finally {
			for (TestExecutorListener listener : testListeners) {
				if (error == null) {
					listener.testExecuted(tc, ctx);
				} else {
					listener.testExceutedWithError(tc, ctx, error);
				}
			}
		}
	}

    public void executeTest(SeleniumTestSuite ts, SeleniumExecutorContext ctx) {

    }

	@Autowired
	public void setCommandListeners(List<CommandExecutorListener> listeners) {
		Collections.sort(listeners, new OrderComparator());
		this.listeners = listeners;
	}

	@Autowired
	public void setTestListeners(List<TestExecutorListener> listeners) {
		Collections.sort(listeners, new OrderComparator());
		this.testListeners = listeners;
	}
}
