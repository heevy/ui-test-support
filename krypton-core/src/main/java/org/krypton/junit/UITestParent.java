package org.krypton.junit;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.krypton.browsers.BrowserDefinition;
import org.krypton.core.SeleniumExecutorContext;
import org.krypton.core.SeleniumTestExecutor;
import org.krypton.core.executors.parser.SeleniumTest;
import org.krypton.core.executors.parser.SeleniumTestParser;
import org.krypton.storage.TestResults;
import org.krypton.support.KryptonTestExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.ContextConfiguration;

import com.thoughtworks.xstream.XStream;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 03.07.13
 * Time: 23:18
 * To change this template use File | Settings | File Templates.
 */
@RunWith(SpringJUnit4ParameterizedClassRunner.class)
@ContextConfiguration(locations = { "classpath:/org/krypton/ui-tests-context.xml" })
public abstract class UITestParent {

	public static final Logger LOG = LoggerFactory.getLogger(KryptonTestExecutor.class);

	public static final String DRIVER_PROPERTY = "krypton.driver";

	protected Class driverClass;

	protected String testCase;

	protected SeleniumTestType testType;

    @Autowired
    private KryptonTestExecutor kryptonTestExecutor;

	protected static Collection<Object[]> getTestSuites(String... testCases) {
		return getTests(SeleniumTestType.TEST_SUITE, testCases);
	}

	protected static Collection<Object[]> getTests(String... testCases) {
		return getTests(SeleniumTestType.TEST_CASE, testCases);
	}

	private static Collection<Object[]> getTests(SeleniumTestType type, String... testCases) {
		List<Object[]> res = new ArrayList<>();
		List<Class> drivers = getDrivers();
		for (String test : testCases) {
			for (Class<? extends WebDriver> driver : drivers) {
				String name = test + "@" + driver.getSimpleName();
				res.add(new Object[] { name, test, type, driver });
			}
		}
		return res;
	}

	protected static List<Class> getDrivers() {
		String drivers = System.getProperty(DRIVER_PROPERTY);
		List<Class> res = new ArrayList<>();
		if (drivers == null || drivers.isEmpty()) {
			LOG.warn("No '{}' has been specified. Using Firefox web driver.", DRIVER_PROPERTY);
			res.add(FirefoxDriver.class);
		} else {
			String[] driverClases = drivers.split(",");
			for (String driverClass : driverClases) {
				try {
					res.add((Class<? extends WebDriver>) Class.forName(driverClass));
				} catch (ClassNotFoundException e) {
					throw new RuntimeException("Cannot create driver : " + driverClass);
				}
			}
		}
		return res;
	}

	protected UITestParent(String name, String testCase, SeleniumTestType aType, Class<? extends WebDriver> driverClass) {
		this.driverClass = driverClass;
		this.testCase = testCase;
		testType = aType;
	}


	@Test
	public void testSelenium() throws Exception {
		kryptonTestExecutor.runTest(testCase,getBaseURL(),testType,driverClass);
	}

    protected String getBaseURL() {
        return null;
    }

}
