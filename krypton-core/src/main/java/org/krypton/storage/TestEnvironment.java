package org.krypton.storage;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 16.07.13
 * Time: 20:13
 * To change this template use File | Settings | File Templates.
 */
public class TestEnvironment {

    private String browserName;

    private String browserVersion;

    private String platform;

    public TestEnvironment(String browserName, String browserVersion, String operationSystem) {
        this.browserName = browserName;
        this.browserVersion = browserVersion;
        this.platform = operationSystem;
    }

    public String getBrowserName() {
        return browserName;
    }

    public String getBrowserVersion() {
        return browserVersion;
    }

    public String getPlatform() {
        return platform;
    }
}
