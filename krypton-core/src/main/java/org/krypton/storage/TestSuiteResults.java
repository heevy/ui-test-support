package org.krypton.storage;

import org.krypton.core.executors.parser.Command;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 16.07.13
 * Time: 17:26
 * To change this template use File | Settings | File Templates.
 */
public class TestSuiteResults {
    private List<TestResults> testResults = new ArrayList<>();
    private String name;

    //for serialization
    protected TestSuiteResults() {
    }

    public TestSuiteResults(String name) {
        this.name = name;
    }

    public List<TestResults> getTestResults() {
        return testResults;
    }
}
