package org.krypton.storage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.krypton.core.executors.parser.Command;
import org.springframework.util.Assert;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 16.07.13
 * Time: 17:26
 * To change this template use File | Settings | File Templates.
 */
public class TestResults {
	private List<CommandStorage> commandStorage = new ArrayList<>();

	private Map<String, Object> testStorage = new HashMap<>();

	private String name;

	// for serialization
	protected TestResults() {
	}

	public TestResults(String name) {
		this.name = name;
	}

	public <T> T getTestStorage(String storageID, Class<T> type) {
		Assert.notNull(storageID);
		Assert.notNull(type);
		return (T) testStorage.get(storageID);
	}

	public void addTestStorage(String storageID, Object storage) {
		Assert.notNull(storageID);
		Assert.notNull(storage);
		testStorage.put(storageID, storage);
	}

	public <T> T getCommandStorage(String storageID, Class<T> type) {
		Assert.notNull(storageID);
		Assert.notNull(type);
		if (commandStorage.isEmpty()) {
			throw new IllegalArgumentException("Command storage is empty.");
		}
		return (T) commandStorage.get(commandStorage.size() - 1).getStorage(storageID, type);
	}

	public void addCommandStorage(String storageID, Object storage) {
		Assert.notNull(storageID);
		Assert.notNull(storage);
		if (commandStorage.isEmpty()) {
			throw new IllegalArgumentException("Command storage is empty.");
		}
		commandStorage.get(commandStorage.size() - 1).addStorage(storageID, storage);
	}

	public void addCommand(Command command) {
		commandStorage.add(new CommandStorage(command));
	}

	public String getName() {
		return name;
	}

	// TODO just for freemarker transfomration. Find better solution
	public Map<String, Object> getTestStorage() {
		return testStorage;
	}

	// TODO just for freemarker transfomration. Find better solution
	public List<CommandStorage> getCommandStorage() {
		return commandStorage;
	}

}
