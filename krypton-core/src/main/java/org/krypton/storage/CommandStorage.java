package org.krypton.storage;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import org.krypton.core.executors.parser.Command;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 07.07.13
 * Time: 10:48
 * To change this template use File | Settings | File Templates.
 */
public class CommandStorage {
	private Command command;

	private boolean executed;

    private Map<String,Object> storage = new HashMap<>();

    /**
     * For serialization.
     */
    private CommandStorage() {

    }

    public CommandStorage(Command command) {
		this.command = command;
		this.executed = true;
	}

	public CommandStorage(Command command, boolean executed) {
		this.command = command;
		this.executed = executed;
	}

	public Command getCommand() {
		return command;
	}

	public boolean isExecuted() {
		return executed;
	}

    public <T> T getStorage(String id, Class<T> storageType){
        return (T) storage.get(id);
    }

    public void addStorage(String id,Object storage){
        this.storage.put(id,storage);
    }

    //TODO just for freemarker transfomration. Find better solution
    public Map<String,Object> getStorage(){
        return storage;
    }
}
