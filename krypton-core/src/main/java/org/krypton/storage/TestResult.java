package org.krypton.storage;

/**
 * Created with IntelliJ IDEA.
 * User: jdk
 * Date: 16.07.13
 * Time: 18:47
 * To change this template use File | Settings | File Templates.
 */
public class TestResult {
    private boolean finished;

    private Throwable exception;

    public TestResult(boolean finshed, Throwable exception) {
        this.finished = finshed;
        this.exception = exception;
    }

    public TestResult(boolean finshed) {
        this.finished = finshed;
    }

    public boolean isFinished() {
        return finished;
    }

    public Throwable getException() {
        return exception;
    }
}
