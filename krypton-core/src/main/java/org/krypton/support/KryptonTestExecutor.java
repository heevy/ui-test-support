package org.krypton.support;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.krypton.browsers.BrowserDefinition;
import org.krypton.core.SeleniumExecutorContext;
import org.krypton.core.SeleniumTestExecutor;
import org.krypton.core.executors.parser.SeleniumTest;
import org.krypton.core.executors.parser.SeleniumTestParser;
import org.krypton.junit.SeleniumTestType;
import org.krypton.storage.TestResults;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.remote.Augmenter;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;

import com.thoughtworks.xstream.XStream;

@Component
public class KryptonTestExecutor {

	public static final Logger LOG = LoggerFactory.getLogger(KryptonTestExecutor.class);

	public static final String DRIVER_PROPERTY = "krypton.driver";

	public static final String BASE_URL_PROPERTY = "krypton.url";

	public static final String RESULT_DIR_PROPERTY = "krypton.result.dir";

	@Value("${" + DRIVER_PROPERTY + ":org.openqa.selenium.firefox.FirefoxDriver}")
	protected String driverProperty;

	@Value("${" + BASE_URL_PROPERTY + ":http://localhost:8080}")
	protected String baseUrl;

	@Value("${" + RESULT_DIR_PROPERTY + ":}")
	protected String resultDir;

	@Value("${krypton.hub.url:http://localhost:4444/wd/hub}")
	protected String hubUrl;

	@Autowired
	protected SeleniumTestExecutor seleniumTestExecutor;

	@Autowired
	protected SeleniumTestParser seleniumTestParser;

	@Autowired
	protected ApplicationContext applicationContext;

	private int maxRetries = 2;

	protected String getBaseURL() {
		// TODO Remove trailing /. It can cause errors.
		String baseURL = System.getProperty(BASE_URL_PROPERTY);
		if (baseURL == null || baseURL.isEmpty()) {
			baseURL = "http://localhost:8080";
			LOG.warn("No '{}' has been specified. Using base url '{}'.", BASE_URL_PROPERTY, baseURL);
			return baseURL;
		}
		return baseURL;
	}

	public List<Class> getConfiguredBrowsers() {
		List<Class> res = new ArrayList<>();
		String[] driverClases = driverProperty.split(",");
		for (String driverClass : driverClases) {
			try {
				res.add((Class<? extends WebDriver>) Class.forName(driverClass));
			} catch (ClassNotFoundException e) {
				throw new RuntimeException("Cannot create driver : " + driverClass);
			}
		}

		return res;
	}

	public void runTest(String testCase, String aBaseUrl, SeleniumTestType aType, Class driverClass) throws Exception {
		int retries = 0;
		boolean done = false;
		do {
			try {
				WebDriver driver = null;
				if (BrowserDefinition.class.isAssignableFrom(driverClass)) {
					BrowserDefinition bd = (BrowserDefinition) driverClass.newInstance();
					DesiredCapabilities dc = bd.getCapabilities();
					driver = new RemoteWebDriver(new URL(hubUrl), dc);
					driver = new Augmenter().augment(driver);
				} else if (RemoteWebDriver.class.isAssignableFrom(driverClass)) {
					driver = (WebDriver) driverClass.newInstance();
				}
				try {
					if (driver instanceof RemoteWebDriver) {
						String browserName = ((RemoteWebDriver) driver).getCapabilities().getBrowserName();
						String browserVersion = ((RemoteWebDriver) driver).getCapabilities().getVersion();
						String browserPlatform = ((RemoteWebDriver) driver).getCapabilities().getPlatform().toString();
						MDC.put("ui.testcase", String.format("%s-%s-%s-%s", testCase, browserPlatform, browserName, browserVersion));
					}
					SeleniumTest seleniumTestCase;
					Resource testResource = new ClassPathResource(testCase);
					if (aType == SeleniumTestType.TEST_CASE) {
						seleniumTestCase = seleniumTestParser.parseTest(testResource);
					} else {
						seleniumTestCase = seleniumTestParser.parseTestSuite(testResource);
					}
					seleniumTestCase.setTitle(testCase);

					TestResults results = new TestResults(testCase);
					String baseUrl = aBaseUrl;
					if (baseUrl == null) {
						baseUrl = getBaseURL();
					}
					SeleniumExecutorContext ctx = new SeleniumExecutorContext(driver, baseUrl, results);
					File resultDir = createTestStorage(seleniumTestCase, ctx);
					File dataDir = createTestFileStorage(resultDir);
					if (dataDir != null) {
						MDC.put("ui.testcase.log", new File(dataDir, "testcase.log").getAbsolutePath());
					}
					ctx.setFileStorage(dataDir);
					try {
						seleniumTestExecutor.executeTest(seleniumTestCase, ctx);
					} finally {
						XStream xstream = new XStream();
						String xml = xstream.toXML(ctx.getResults());
						FileUtils.write(new File(resultDir, "krypton-test-result.xml"), xml, "UTF-8");
					}
					done = true;
				} finally {
					driver.quit();
				}
			} catch (WebDriverException|AssertionError e) {
				retries++;
				done = retries == maxRetries;
				LOG.error(String.format("Test has failed. Retry. Retries : %s Max retries : %s", retries, maxRetries),e);
                if(done){
                    throw e;
                }
			} finally {
				MDC.clear();
			}
		} while (!done);
	}

	private File createTestFileStorage(File aTestStorage) {
		if (aTestStorage == null) {
			return null;
		}
		File dir = new File(aTestStorage, "data");
		dir.mkdir();
		return dir;
	}

	private File createTestStorage(SeleniumTest aTest, SeleniumExecutorContext ctx) {
		String outputFolder = System.getProperty(RESULT_DIR_PROPERTY);
		if (outputFolder == null) {
			return null;
		}
		String browserName = ctx.getDriver().getClass().getSimpleName();
		String browserVersion = null;
		String browserPlatform = null;
		String testFolder = null;
		String testPath = aTest.getTitle();
		if (testPath.startsWith("/")) {
			testPath = testPath.substring(1);
		}
		testPath = testPath.replace("/", File.separator);
		if (ctx.getDriver() instanceof RemoteWebDriver) {
			browserName = ((RemoteWebDriver) ctx.getDriver()).getCapabilities().getBrowserName();
			browserVersion = ((RemoteWebDriver) ctx.getDriver()).getCapabilities().getVersion();
			browserPlatform = ((RemoteWebDriver) ctx.getDriver()).getCapabilities().getPlatform().toString();
			testFolder = concatFilename(outputFolder, testPath, browserPlatform, browserName, browserVersion);
		} else {
			testFolder = concatFilename(outputFolder, testPath, browserName);
		}
		File f = new File(testFolder);
		f.mkdirs();
		return f;
	}

	private String concatFilename(String... parts) {
		String res = parts[0];
		for (int i = 1; i < parts.length; i++) {
			res = FilenameUtils.concat(res, parts[i]);
		}
		return res;
	}
}
