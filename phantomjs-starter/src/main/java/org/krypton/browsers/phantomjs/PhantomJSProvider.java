package org.krypton.browsers.phantomjs;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.zip.ZipInputStream;

public class PhantomJSProvider {

    private static String OS = System.getProperty("os.name").toLowerCase();

	private static final String PREFIX = "/org/krypton/browsers/phantomjs/executable/phantomjs_";

	private Path executable = null;
	private static boolean executableWritten = false;


	private Logger log = LoggerFactory.getLogger(PhantomJSProvider.class.getName());

	private String getArchitecture() {
        if(isLinux()){
            return "linux";
        }
        if(isMac()){
            return "mac";
        }
        if(isWindows()){
            return "win";
        }
        throw new RuntimeException("OS is not supported : "+OS);
	}

	private InputStream getResourceStreamByArchitecture() {
		String architecture = getArchitecture();
		String resourceName = PREFIX + architecture+".zip";
        InputStream is = getClass().getResourceAsStream(resourceName);
        ZipInputStream zis = new ZipInputStream(is);
        try {
            zis.getNextEntry();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return zis;
	}

	private String getSuffixByArchitecture() {
		return "";
	}

	private Path initializeExecutable(Path tempStorage) {
		try {
            Path executable = tempStorage.resolve("phantomjs"+getSuffixByArchitecture());
            if(Files.exists(executable)){
                return executable;
            }
			executable = Files.createFile(executable);
            executable.toFile().setExecutable(true);
            executable.toFile().setReadable(true);
			executable.toFile().deleteOnExit();
			return executable;
		} catch (IOException e) {
			throw new IllegalStateException("Can't initialize PhantomJSProvider executable", e);
		}
	}

	public Path getPhantomJS(Path tempStorage) {
		if (!executableWritten) {
			synchronized (this) {
				if (!executableWritten) {
                    executable=initializeExecutable(tempStorage);
					writeStreamToFile(getResourceStreamByArchitecture(), executable);
					executableWritten = true;
				}
			}
		}
		return executable;
	}

	private void writeStreamToFile(InputStream inputStream, Path file) {
		try {
			byte[] buffer = new byte[8192];
			OutputStream fileOutputStream = Files.newOutputStream(file);

			int r;
			while ((r = inputStream.read(buffer)) != -1) {
				fileOutputStream.write(buffer, 0, r);
			}
			fileOutputStream.close();
			inputStream.close();
		} catch (IOException e) {
			throw new IllegalStateException("Can't write stream to file", e);
		}

	}

    private static boolean isWindows() {
        return (OS.indexOf("win") >= 0);
    }

    private static boolean isMac() {
        return (OS.indexOf("mac") >= 0);
    }

    private static boolean isLinux() {
        return (OS.indexOf("linux") >= 0);
    }
}
